package case8;

import case8.entities.PersonCase8;
import common.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;

/**
 * Pessimistic lock in Oracle.
 * <p/>
 * Created by Viacheslav on 13.08.15.
 */
public class Main {

    public static void main(String[] args) {
        EntityManager em = PersistenceUtils.openConnection();


        /** SELECT ... FOR UPDATE. */
        String sql = "from PersonCase8 p where p.id = 38";
        TypedQuery<PersonCase8> query = em.createQuery(sql, PersonCase8.class);
        query.setLockMode(LockModeType.PESSIMISTIC_WRITE);

        query.getResultList();


        PersistenceUtils.closeConnection(em);
    }
}
