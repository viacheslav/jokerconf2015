package case8.entities;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Viacheslav on 10.08.15.
 */
@Entity
@Table(name = "case1_person")
public class PersonCase8 {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id", updatable = false)
    private Set<PetCase8> pets;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<PetCase8> getPets() {
        return pets;
    }

    public void setPets(Set<PetCase8> pets) {
        this.pets = pets;
    }
}
