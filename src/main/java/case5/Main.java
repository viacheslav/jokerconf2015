package case5;

import case5.entities.PersonCase5;
import common.PersistenceUtils;

import javax.persistence.EntityManager;

/**
 * First level cache.
 * <p/>
 * Created by Viacheslav on 16.08.15.
 */
public class Main {


    public static void main(String[] args) throws InterruptedException {
        EntityManager em = PersistenceUtils.openConnection();


        /** Find a person by PK. */
        PersonCase5 p = em.find(PersonCase5.class, 38);
        System.out.println("*********** Person name before update: " + p.getName());

        /** Find one more time, without DB access. */
        p = em.find(PersonCase5.class, 38);
        System.out.println("***********  Person name after update: " + p.getName());


        PersistenceUtils.closeConnection(em);
    }
}
