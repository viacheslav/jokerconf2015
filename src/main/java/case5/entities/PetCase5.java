package case5.entities;

import javax.persistence.*;

/**
 * Created by Viacheslav on 10.08.15.
 */
@Entity
@Table(name = "case1_pet")
public class PetCase5 {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false, updatable = false)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id")
    private PersonCase5 owner;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PersonCase5 getOwner() {
        return owner;
    }

    public void setOwner(PersonCase5 owner) {
        this.owner = owner;
    }
}
