package case5;

import case5.entities.PersonCase5;
import common.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Viacheslav on 23.08.15.
 */
public class Main2 {

    public static void main(String[] args) {
        EntityManager em1 = PersistenceUtils.openConnection();


        /** Find a person. */
        String jpql = "from PersonCase5 p where p.id = 38";

        TypedQuery<PersonCase5> query = em1.createQuery(jpql, PersonCase5.class);
        List<PersonCase5> resultList = query.getResultList();

        /** Update the name. */
        PersonCase5 p = resultList.get(0);
        p.setName("NOT REAL");

        em1.merge(p);


        PersistenceUtils.closeConnection(em1);
    }
}
