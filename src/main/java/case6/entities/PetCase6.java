package case6.entities;

import javax.persistence.*;

/**
 * Created by Viacheslav on 10.08.15.
 */
@Entity
@Table(name = "case1_pet")
public class PetCase6 {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false, updatable = false)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id")
    private PersonCase6 owner;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PersonCase6 getOwner() {
        return owner;
    }

    public void setOwner(PersonCase6 owner) {
        this.owner = owner;
    }
}
