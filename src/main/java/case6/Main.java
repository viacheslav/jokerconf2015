package case6;

import case6.entities.PersonCase6;
import common.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.Query;
import java.util.List;

/**
 * Native and JPQL queries.
 *
 * ***
 * Before run
 * ***
 *
 * Created by Viacheslav on 23.08.15.
 */
public class Main {

    private static String EXISTENCE_CHECK = "SELECT case1_person.* FROM case1_person left join case1_pet on case1_person.id = case1_pet.person_id WHERE case1_person.name = 'Kevin'";

    public static void main(String[] args) {
        EntityManager em = PersistenceUtils.openConnection();
        em.setFlushMode(FlushModeType.COMMIT);



        for (int i = 0; i < 5; i++) {
            Query nativeQuery = em.createNativeQuery(EXISTENCE_CHECK);
            List resultList = nativeQuery.getResultList();
            if (resultList.isEmpty()) {
                PersonCase6 person = em.find(PersonCase6.class, 38);
                person.setName("Kevin");
            }
        }


        PersistenceUtils.closeConnection(em);
    }


}
