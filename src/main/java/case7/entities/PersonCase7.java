package case7.entities;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Viacheslav on 10.08.15.
 */
@Entity
@Table(name = "case1_person")
public class PersonCase7 {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id", updatable = false)
    private Set<PetCase7> pets;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<PetCase7> getPets() {
        return pets;
    }

    public void setPets(Set<PetCase7> pets) {
        this.pets = pets;
    }
}
