package case7;

import case7.entities.PersonCase7;
import common.PersistenceUtils;

import javax.persistence.EntityManager;

/**
 * Get reference.
 * <p/>
 * Created by Viacheslav on 14.08.15.
 */
public class Main {

    public static void main(String[] args) {
        EntityManager em = PersistenceUtils.openConnection();


        PersonCase7 reference = em.getReference(PersonCase7.class, 1900);
        reference.getId();


        PersistenceUtils.closeConnection(em);
    }
}
