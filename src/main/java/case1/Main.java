package case1;

import case1.entities.PersonCase1;
import common.PersistenceUtils;

import javax.persistence.EntityManager;

/**
 * Error messages.
 * <p/>
 * Created by Viacheslav on 10.08.15.
 */
public class Main {

    public static void main(String[] args) {
        EntityManager em = PersistenceUtils.openConnection();

        /** Create a new person. */
        PersonCase1 person = new PersonCase1();

        /** The field is really int in the DB.  */
        person.setNumberValue("TEST");
        em.persist(person);


        PersistenceUtils.closeConnection(em);
    }
}
