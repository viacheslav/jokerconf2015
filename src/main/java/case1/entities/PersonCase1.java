package case1.entities;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Viacheslav on 10.08.15.
 */
@Entity
@Table(name = "case1_person")
public class PersonCase1 {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    @Column(name = "numberValue")
    private String numberValue;

    public String getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(String numberValue) {
        this.numberValue = numberValue;
    }

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id", updatable = false)
    private Set<PetCase1> pets;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<PetCase1> getPets() {
        return pets;
    }

    public void setPets(Set<PetCase1> pets) {
        this.pets = pets;
    }
}
