package case3.entities;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Viacheslav on 13.08.15.
 */
@Entity
@Table(name = "case5_VeryVeryImportantAndSuperiorPerson")
public class PersonCase3 {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<PetCase3> pets;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PetCase3> getPets() {
        return pets;
    }

    public void setPets(List<PetCase3> pets) {
        this.pets = pets;
    }
}
