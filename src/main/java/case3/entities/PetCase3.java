package case3.entities;

import javax.persistence.*;

/**
 * Created by Viacheslav on 13.08.15.
 */
@Entity
@Table(name = "case5_LuxuryAndIncrediblyBeautifulPet")
public class PetCase3 {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id", updatable = false)
    private PersonCase3 owner;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PersonCase3 getOwner() {
        return owner;
    }

    public void setOwner(PersonCase3 owner) {
        this.owner = owner;
    }
}
