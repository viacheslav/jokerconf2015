package case3;

import case3.entities.PersonCase3;
import case3.entities.PetCase3;
import common.PersistenceUtils;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Default names.
 *
 * Created by Viacheslav on 14.08.15.
 */
public class Main {

    public static void main(String[] args) {
        EntityManager em = PersistenceUtils.openConnection();


        /** Create a new person. */
        PersonCase3 person = new PersonCase3();

        /** Create a pet for the person and link them. */
        List<PetCase3> pets = new ArrayList<>();
        PetCase3 pet = new PetCase3();
        pet.setName("Name");
        pets.add(pet);
        person.setPets(pets);

        em.persist(person);


        PersistenceUtils.closeConnection(em);
    }
}
