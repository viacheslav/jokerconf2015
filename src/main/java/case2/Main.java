package case2;

import case2.entities.PersonCase2;
import case2.entities.PetCase2;
import common.PersistenceUtils;

import javax.persistence.EntityManager;
import java.util.HashSet;
import java.util.Set;

/**
 * Updates on inserts.
 *
 * Created by Viacheslav on 13.08.15.
 */
public class Main {

    public static void main(String[] args) {
        EntityManager em = PersistenceUtils.openConnection();


        /** Create a new person. */
        PersonCase2 person = new PersonCase2();
        person.setName("Alex");

        /** Create a couple of pets for the person. */
        PetCase2 pet = new PetCase2();
        pet.setName("Bob");

        PetCase2 pet1 = new PetCase2();
        pet1.setName("Tom");

        /** Link them. */
        Set<PetCase2> pets = new HashSet<>();
        pets.add(pet);
        pets.add(pet1);
        person.setPets(pets);

        em.persist(person);


        PersistenceUtils.closeConnection(em);
    }
}
