package case2.entities;

import javax.persistence.*;

/**
 * Created by Viacheslav on 10.08.15.
 */
@Entity
@Table(name = "case1_pet")
public class PetCase2 {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false, updatable = false)
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
