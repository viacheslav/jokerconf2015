package case2.entities;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Viacheslav on 10.08.15.
 */
@Entity
@Table(name = "case1_person")
public class PersonCase2 {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id")
    private Set<PetCase2> pets;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<PetCase2> getPets() {
        return pets;
    }

    public void setPets(Set<PetCase2> pets) {
        this.pets = pets;
    }
}
