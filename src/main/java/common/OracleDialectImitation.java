package common;

import org.hibernate.dialect.MySQL5Dialect;

/**
 * Created by Viacheslav on 13.08.15.
 */
public class OracleDialectImitation extends MySQL5Dialect {

    @Override
    public boolean useFollowOnLocking() {
        return true;
    }
}
