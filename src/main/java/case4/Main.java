package case4;

import case4.entities.PersonCase4;
import common.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 * SetMaxResults.
 *
 * Created by Viacheslav on 13.08.15.
 */
public class Main {

    public static void main(String[] args) {
        EntityManager em = PersistenceUtils.openConnection();


        String sql = "SELECT p FROM PersonCase4 p JOIN FETCH p.pets AS pets";
//        For proposed solution
//        String sql = "SELECT p FROM PersonCase4 p";

        TypedQuery<PersonCase4> query = em.createQuery(sql, PersonCase4.class);
        query.setMaxResults(1);
        query.getResultList();


        PersistenceUtils.closeConnection(em);
    }
}
