package case4.entities;


import javax.persistence.*;
import java.util.Set;

/**
 * Created by Viacheslav on 10.08.15.
 */
@Entity
@Table(name = "case1_person")
public class PersonCase4 {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id", updatable = false)
//    @Fetch(FetchMode.SELECT)
//    @BatchSize(size = 2)
    private Set<PetCase4> pets;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<PetCase4> getPets() {
        return pets;
    }

    public void setPets(Set<PetCase4> pets) {
        this.pets = pets;
    }
}
